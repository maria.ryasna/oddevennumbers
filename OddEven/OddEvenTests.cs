﻿using System;
using System.Collections.Generic;
using Xunit;

namespace OddEven
{
    public class OddEvenTests
    {
        [Theory]
        [InlineData(22)]
        [InlineData(4)]
        [InlineData(56)]
        public void ShouldPrintEvenInsteadOfNumber(int value)
        {
            // Arrange
            string expected = "Even"; 

            // Act
            string actual = OddEvenService.GetValue(value);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(9)]
        [InlineData(15)]
        [InlineData(27)]
        public void ShouldPrintOddInsteadOfNumber(int value)
        {
            // Arrange
            string expected = "Odd";

            // Act
            string actual = OddEvenService.GetValue(value);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(3)]
        [InlineData(7)]
        [InlineData(13)]
        public void ShouldPrintNumberIfItIsPrime(int value)
        {
            // Arrange
            string expected = value.ToString();

            // Act
            string actual = OddEvenService.GetValue(value);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ShouldThrowArgumentIndexOutOfRangeExceptionIfNumberLessOne()
        {
            // Arrange
            int value = -5;

            // Act
            Func<string> actual = () => OddEvenService.GetValue(value);

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(actual);
        }

        [Theory]
        [InlineData(23)]
        [InlineData(53)]
        [InlineData(67)]
        public void ShouldReturnTrueIfItIsPrime(int value)
        {
            // Act
            bool actual = OddEvenService.IsPrime(value);

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public void ShouldPrintResultFromRange()
        {
            // Arrange
            int from = 1, to = 4;
            List<string> expected = new List<string> {"1", "2", "3", "Even"};

            // Act
            List<string> actual = OddEvenService.GetValuesFromRange(from, to);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ShouldPrintOneValueFromRangeIfFromAndToTheSame()
        {
            // Arrange
            int from = 4, to = 4;
            List<string> expected = new List<string> { "Even" };

            // Act
            List<string> actual = OddEvenService.GetValuesFromRange(from, to);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ShouldThrowArgumentExceptionIfIncorrectRange()
        {
            // Arrange
            int from = 5, to = 3;

            // Act
            Func<List<string>> actual = () => OddEvenService.GetValuesFromRange(from, to);

            // Assert
            Assert.Throws<ArgumentException>(actual);
        }
    }
}
