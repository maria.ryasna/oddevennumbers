﻿using System;
using System.Collections.Generic;

namespace OddEven
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> numbers = OddEvenService.GetValuesFromRange(1, 100);
            foreach (string number in numbers)
            {
                Console.Write($"{number} ");
            }
            Console.ReadKey();
        }
    }
}
