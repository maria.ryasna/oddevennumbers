﻿using System;
using System.Collections.Generic;

namespace OddEven
{
    public static class OddEvenService
    {
        public static string GetValue(int value)
        {   
            if (value < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }
            if (IsPrime(value))
            {
                return value.ToString();
            }
            return value % 2 == 0 ? "Even" : "Odd";
        }

        public static bool IsPrime(int value)
        {
            for (int number = 1; number <= value; number++)
            {
                if (value % number == 0 && number != 1 && value != number)
                {
                    return false;
                }
            }
            return true;
        }

        public static List<string> GetValuesFromRange(int from, int to)
        {
            if (from > to)
            {
                throw new ArgumentException(nameof(from));
            }
            List<string> array = new List<string>(to - from);
            for (int number = from; number <= to; number++)
            {
                var value = GetValue(number);
                array.Add(value);
            }
            return array;
        }
    }
}